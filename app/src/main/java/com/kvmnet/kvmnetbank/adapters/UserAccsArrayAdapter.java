package com.kvmnet.kvmnetbank.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kvmnet.kvmnetbank.R;
import com.kvmnet.kvmnetbank.pojo.Cuenta;

import java.util.List;

public class UserAccsArrayAdapter<T> extends ArrayAdapter<T> {

     //Stock constructor
    public UserAccsArrayAdapter(@NonNull Context context, @NonNull List<T> objects) {
        super(context, 0, objects);
    }

    //Get data for this position
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        //Get data item for this position
        Cuenta cuenta = (Cuenta)getItem(position);

        //check if an existing view is being used, otherwise inflate with our xml
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.user_accounts_list, parent, false);
        }

        //Lookup view for data
        TextView accBankName = (TextView)convertView.findViewById(R.id.textView_bank_name);
        TextView accBankBalance = (TextView)convertView.findViewById(R.id.textView_account_balance);
        TextView accBankNumber = (TextView)convertView.findViewById(R.id.textView_account_number);

        accBankName.setText(cuenta.getBanco());
        accBankBalance.setText(String.valueOf(cuenta.getSaldoActual()));
        accBankNumber.setText(cuenta.getNumeroCuenta());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //Get data item for this position
        Cuenta cuenta = (Cuenta)getItem(position);

        //check if an existing view is being used, otherwise inflate with our xml
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.user_accounts_list, parent, false);
        }

        //Lookup view for data
        TextView accBankName = (TextView)convertView.findViewById(R.id.textView_bank_name);
        TextView accBankBalance = (TextView)convertView.findViewById(R.id.textView_account_balance);
        TextView accBankNumber = (TextView)convertView.findViewById(R.id.textView_account_number);

        accBankName.setText(cuenta.getBanco());
        accBankBalance.setText(String.valueOf(cuenta.getSaldoActual()));
        accBankNumber.setText(cuenta.getNumeroCuenta());

        return convertView;
    }
}
