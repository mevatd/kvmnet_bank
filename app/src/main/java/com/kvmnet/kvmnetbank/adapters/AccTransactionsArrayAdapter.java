package com.kvmnet.kvmnetbank.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kvmnet.kvmnetbank.R;
import com.kvmnet.kvmnetbank.pojo.Movimiento;

import java.util.List;

public class AccTransactionsArrayAdapter<T> extends ArrayAdapter {

    public AccTransactionsArrayAdapter(@NonNull Context context, @NonNull List objects) {
        super(context, 0, objects);
    }


    @NonNull
    @Override
    public View getView(int position,  @Nullable View convertView, @NonNull ViewGroup parent) {

        //get movimiento for this position
        Movimiento movimiento = (Movimiento)getItem(position);

        //inflate with our xml if no view is used
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.user_account_transactions, parent, false);
        }

        //connect view with data
        TextView transactionDesc = (TextView)convertView.findViewById(R.id.textView_transaction_desc);
        TextView transactionAmount = (TextView)convertView.findViewById(R.id.textView_transaction_amount);

        transactionDesc.setText(movimiento.getDescripcion());
        transactionAmount.setText(String.valueOf(movimiento.getImporte()));

        return convertView;
    }
}
