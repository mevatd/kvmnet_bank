package com.kvmnet.kvmnetbank.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;


import com.kvmnet.kvmnetbank.R;
import com.kvmnet.kvmnetbank.bd.Constantes;
import com.kvmnet.kvmnetbank.dao.CajeroDAO;

import static com.kvmnet.kvmnetbank.bd.Constantes.*;

public class CajeroCursorAdapter extends CursorAdapter {

    private CajeroDAO cajeroDAO = null;

    public CajeroCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
        cajeroDAO = new CajeroDAO(context);
        cajeroDAO.open();
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View view = inflater.inflate(R.layout.atm_list, parent, false);


        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView textViewAtmId = view.findViewById(R.id.textView_atmId);
        TextView textViewAtmDir = view.findViewById(R.id.textView_atmDirection);
        TextView textViewAtmLat = view.findViewById(R.id.textView_atmLat);
        TextView textViewAtmLng = view.findViewById(R.id.textView_atmLng);

        int id = cursor.getColumnIndex(FIELD_CAJEROS_ID);

        int dir = cursor.getColumnIndex(Constantes.FIELD_DIRECCION);
        int lat = cursor.getColumnIndex(Constantes.FIELD_LAT);
        int lng = cursor.getColumnIndex(Constantes.FIELD_LNG);

        textViewAtmId.setText(cursor.getString(id));
        textViewAtmDir.setText(cursor.getString(dir));
        textViewAtmLat.setText(cursor.getString(lat));
        textViewAtmLng.setText(cursor.getString(lng));

    }
}
