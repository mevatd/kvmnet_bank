package com.kvmnet.kvmnetbank.dialogs;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.kvmnet.kvmnetbank.R;
import com.kvmnet.kvmnetbank.activities.AtmActivity;
import com.kvmnet.kvmnetbank.bd.Constantes;
import com.kvmnet.kvmnetbank.bd.MiBD;
import com.kvmnet.kvmnetbank.bd.MiBancoOperacional;
import com.kvmnet.kvmnetbank.dao.CajeroDAO;
import com.kvmnet.kvmnetbank.pojo.Movimiento;

public class AtmDialogInfo extends DialogFragment {

    private Cursor c;
    private MiBancoOperacional mbo;
    private CajeroDAO cajeroDAO;

    TextView textViewId;
    TextView textViewDir;
    TextView textViewLat;
    TextView textViewLng;
    TextView textViewZoom;

    String id;
    String dir;
    String lat;
    String lng;
    String zoom;

    Button btnDismiss;
    Button btnModify;
    Button btnDelete;


    public static AtmDialogInfo newInstance(Cursor cursor, long idl) {

        AtmDialogInfo atmDialogInfo = new AtmDialogInfo();

        String id = cursor.getString(cursor.getColumnIndex(Constantes.FIELD_CAJEROS_ID));
        String dir = cursor.getString(cursor.getColumnIndex(Constantes.FIELD_DIRECCION));
        String lat = cursor.getString(cursor.getColumnIndex(Constantes.FIELD_LAT));
        String lng = cursor.getString(cursor.getColumnIndex(Constantes.FIELD_LNG));
        //String zoom=cursor.getString(cursor.getColumnIndex(Constantes.FIELD_ZOOM));

        Bundle bundle = new Bundle();

        bundle.putString("id", id);
        bundle.putString("dir", dir);
        bundle.putString("lat", lat);
        bundle.putString("lng", lng);
        bundle.putLong("idl", idl);


        atmDialogInfo.setArguments(bundle);


        return atmDialogInfo;
    }

    //set our custom layout
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        return inflater.inflate(R.layout.activity_atm_dialog, container);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //get object from bundle

        id = getArguments().getString("id");
        dir = getArguments().getString("dir");
        lat = getArguments().getString("lat");
        lng = getArguments().getString("lng");

        //get fields from view to set text
        TextView title = view.findViewById(R.id.textView_header_info_transaction);
        title.setText("Cajero " + id );

        textViewId = view.findViewById(R.id.textView_atmId);
        textViewId.setText(id);

        textViewDir = view.findViewById(R.id.textView_atmDirection);
        textViewDir.setText(dir);

        textViewLat = view.findViewById(R.id.textView_atmLat);
        textViewLat.setText(lat);

        textViewLng = view.findViewById(R.id.textView_atmLng);
        textViewLng.setText(lng);

        btnDismiss = view.findViewById(R.id.button_dismiss);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              getDialog().dismiss();
                                          }
                                      }
        );

        btnModify = view.findViewById(R.id.button_modify);
        btnModify.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              textViewDir.setEnabled(true);
                                          }
                                      }
        );

        btnDelete = view.findViewById(R.id.button_delete);
        btnDelete.setOnClickListener(new View.OnClickListener() {
                                         @Override
                                         public void onClick(View v) {
                                             //Get an instance from DB
                                             mbo = MiBancoOperacional.getInstance(getActivity().getBaseContext());
                                             cajeroDAO = new CajeroDAO(getContext());
                                             cajeroDAO.open();
                                             cajeroDAO.delete(getArguments().getLong("idl"));
                                             cajeroDAO.close();


                                             AtmActivity atma = (AtmActivity) getActivity();
                                             //atma.onUpdate();
                                             getDialog().dismiss();

                                             // NO SE DEBE DE HACER ASI
                                             atma.recreate();


                                         }
                                     }
        );



    }
}
