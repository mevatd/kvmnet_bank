package com.kvmnet.kvmnetbank.dialogs;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.kvmnet.kvmnetbank.R;
import com.kvmnet.kvmnetbank.pojo.Movimiento;

public class TransactionsDialog extends DialogFragment {

    TextView conceptTrans;
    TextView type;
    TextView date;
    TextView accOrigin;
    TextView accDestination;

    Button btnDismiss;


    public static TransactionsDialog newInstance(Movimiento movimiento) {

        TransactionsDialog transactionsDialog = new TransactionsDialog();

        Bundle bundle = new Bundle();

        bundle.putSerializable("transaction", movimiento);

        transactionsDialog.setArguments(bundle);


        return transactionsDialog;
    }

    //set our custom layout
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        return inflater.inflate(R.layout.activity_transactions_dialog, container);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //get object from bundle
        Movimiento m = (Movimiento) getArguments().getSerializable("transaction");

        //get fields from view to set text
        conceptTrans = view.findViewById(R.id.textView_header_info_transaction);
        conceptTrans.setText(m.getDescripcion());

        type = view.findViewById(R.id.textView_type);
        type.setText(String.valueOf(m.getTipo()));

        date = view.findViewById(R.id.textView_date);
        date.setText(m.getFechaOperacion().toString());

        accOrigin = view.findViewById(R.id.textView_account_origin);
        accOrigin.setText(m.getCuentaOrigen().getNumeroCuenta());

        accDestination = view.findViewById(R.id.textView_account_destination);
        accDestination.setText(m.getCuentaDestino().getNumeroCuenta());

        btnDismiss = view.findViewById(R.id.button_dismiss);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              getDialog().dismiss();
                                          }
                                      }
        );
    }
}
