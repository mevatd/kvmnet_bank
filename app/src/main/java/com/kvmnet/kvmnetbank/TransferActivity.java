package com.kvmnet.kvmnetbank;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.kvmnet.kvmnetbank.adapters.UserAccsArrayAdapter;
import com.kvmnet.kvmnetbank.bd.MiBancoOperacional;
import com.kvmnet.kvmnetbank.pojo.Cliente;
import com.kvmnet.kvmnetbank.pojo.Cuenta;
import com.kvmnet.kvmnetbank.pojo.Movimiento;

import java.util.ArrayList;
import java.util.Calendar;

public class TransferActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener, View.OnClickListener {

    //aux for toasts
    String info;
    String acc;
    Boolean ownAcc = false;
    int defaultPos;


    Button btnAccept;
    Button btnCancel;

    CheckBox chkBoxSendProof;

    LinearLayout loDestAccDetails;

    ConstraintLayout loTransfer;

    EditText txtAlienAcc;

    GridView gridViewOriginAccount;

    RadioGroup rGrpDestAcc;

    EditText txtAmount;

    Spinner spnrCurrency;
    Spinner spnrOwnAcc;

    MiBancoOperacional mbo;
    ArrayAdapter<String> currAdapter;
    ArrayAdapter<Cuenta> accsAdapter;
    ArrayList<Cuenta> cuentasPositivas = new ArrayList<>();
    ArrayList<Cuenta> cuentas = new ArrayList<>();
    Cliente cliente;
    Cuenta cuentaOrigen;
    Cuenta cuentaDestino;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer);

        mbo = MiBancoOperacional.getInstance(this);

        Bundle bundle = getIntent().getExtras();

        cliente = (Cliente) bundle.getSerializable("Client");

        btnAccept = findViewById(R.id.BtnAcceptTransfer);
        btnAccept.setOnClickListener(this);

        btnCancel = findViewById(R.id.BtnCancelTransfer);
        btnCancel.setOnClickListener(this);


        //show user's accounts
        gridViewOriginAccount = findViewById(R.id.GridView_origin_account);

        cuentas = mbo.getCuentas(cliente);
        for (Cuenta c : cuentas) {
            if (c.getSaldoActual() > 0) {
                cuentasPositivas.add(c);
            }
        }
        accsAdapter = new UserAccsArrayAdapter<>(this, cuentasPositivas);
        gridViewOriginAccount.setAdapter(accsAdapter);

        defaultPos = gridViewOriginAccount.getFirstVisiblePosition();

        gridViewOriginAccount.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //get selected origin account
                cuentaOrigen = (Cuenta) parent.getItemAtPosition(position);
                info = "From: " + cuentaOrigen.toString();

            }
        });

        //group radio buttons to decide destination account
        rGrpDestAcc = findViewById(R.id.RGrpDestAcc);
        rGrpDestAcc.setOnCheckedChangeListener(this);

        chkBoxSendProof = findViewById(R.id.ChkBoxSendProof);

        //layout that contains destination account details
        loDestAccDetails = findViewById(R.id.LoDestAccDetails);

        //show different currecny options in a spinner
        spnrCurrency = findViewById(R.id.SpnrCurrency);

        //show own accounts as destinations in a spinner
        spnrOwnAcc = findViewById(R.id.SpnrOwnAcc);

        //show txt to introduce alien's account number
        txtAlienAcc = findViewById(R.id.TxtAlienAcc);

        //show txt to introduce amount to transfer
        txtAmount = findViewById(R.id.TxtAmount);

        loTransfer = findViewById(R.id.LoTransfer);

    }


    //radio button group composed of two radio buttons
    //shows spinner if own account destionation is selected
    //show edit text if alien account destination is selected
    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        loDestAccDetails.setVisibility(View.VISIBLE);

        currAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, AccountData.CURRENCIES);
        spnrCurrency.setAdapter(currAdapter);

        chkBoxSendProof.setVisibility(View.VISIBLE);

        switch (checkedId) {

            case R.id.RBtnOwnDestAcc:
                txtAlienAcc.setVisibility(View.GONE);

                spnrOwnAcc.setVisibility(View.VISIBLE);

                accsAdapter = new UserAccsArrayAdapter<>(this, cuentas);
                spnrOwnAcc.setAdapter(accsAdapter);
                ownAcc = true;
                break;
            case R.id.RBtnAlienDestAcc:
                spnrOwnAcc.setVisibility(View.GONE);

                txtAlienAcc.setVisibility(View.VISIBLE);
                ownAcc = false;
                break;

            default:
                Toast.makeText(this, "Not implemented yet", Toast.LENGTH_SHORT).show();
        }
    }

    //Accept and Cancel buttons
    @Override
    public void onClick(View v) {

        //control to get correct destination account from previous steps
        if (ownAcc) {
            cuentaDestino = (Cuenta) spnrOwnAcc.getSelectedItem();
            acc = cuentaDestino.toString();
        } else {

            acc = txtAlienAcc.getText().toString();
        }
        //infoAux is in case we double click accept we would not dupicate the information till now.
        String infoAux = info;

        switch (v.getId()) {
            case R.id.BtnAcceptTransfer:

                if (cuentaOrigen != null) {


                    infoAux += " " + txtAmount.getText() + spnrCurrency.getSelectedItem() + " to " + acc;
                    Toast.makeText(this, infoAux, Toast.LENGTH_SHORT).show();

                    int lastIdO = mbo.getMovimientos(cuentaOrigen).get(mbo.getMovimientos(cuentaOrigen).size() - 1).getId() + 1;

                    Movimiento movimientoOwn = new Movimiento(lastIdO, 0, Calendar.getInstance().getTime(), "Desc generica - pruebas", Integer.parseInt(txtAmount.getText().toString()), cuentaOrigen, cuentaDestino);

                    int transferCode = mbo.transferencia((movimientoOwn));
                    if (transferCode == 0) {
                        System.out.println("TODO OK");
                    } else if (transferCode == 1) {
                        System.out.println("La cuenta no existe");
                    } else if (transferCode == 2){
                        System.out.println("La cuenta no dispone de saldo suficiente");
                    }

                } else {
                    Toast.makeText(this, "No origin account is selected", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.BtnCancelTransfer:
                //we dont reset txt edits just for comodity
                //because the user may press cancel button by error and
                //it will be a pain to have to type again the account which
                //is pretty long
                gridViewOriginAccount.smoothScrollToPosition(defaultPos);
                rGrpDestAcc.clearCheck();
                chkBoxSendProof.setChecked(false);
                loDestAccDetails.setVisibility(View.GONE);
                chkBoxSendProof.setVisibility(View.GONE);
                txtAlienAcc.setVisibility(View.GONE);
                spnrOwnAcc.setVisibility(View.GONE);
                Toast.makeText(this, "Transfer canceelled", Toast.LENGTH_SHORT).show();
                break;

            default:

                Toast.makeText(this, "Not implemented yet", Toast.LENGTH_SHORT).show();

        }

    }
}
