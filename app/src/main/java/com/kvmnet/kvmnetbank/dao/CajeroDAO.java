package com.kvmnet.kvmnetbank.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kvmnet.kvmnetbank.bd.MiBD;

import static com.kvmnet.kvmnetbank.bd.Constantes.*;

public class CajeroDAO {

    private Context context;
    private MiBD miBD;
    private SQLiteDatabase db;

    public CajeroDAO(Context context) {
        this.context = context;
    }

    public CajeroDAO open() {
        miBD = MiBD.getInstance(context);
        db = miBD.getWritableDatabase();
        return this;
    }

    public void close() {
        miBD.close();
    }


    public long add(ContentValues reg) {
        return db.insert
                (
                        CAJEROS_TABLE,
                        null,
                        reg
                );
    }


    public long update(ContentValues reg) {
        long result = 0;
        if (reg.containsKey(FIELD_CAJEROS_ID)) {

            //Quitamos el ID de los valores a actualizar
            // porque el ID no se actualiza
            long id = reg.getAsLong(FIELD_CAJEROS_ID);

            reg.remove(FIELD_CAJEROS_ID);

            //Actualizamos
            result = db.update
                    (
                            CAJEROS_TABLE,
                            reg,
                            FIELD_CAJEROS_ID + id,
                            null
                    );
        }
        return result;
    }

    public long delete(long _id) {
        return db.delete
                (
                        CAJEROS_TABLE,
                        "_id=" + _id,
                        null
                );
    }

    public Cursor getCursor() {
        Cursor c = db.query
                (
                        true,
                        CAJEROS_TABLE,
                        CAMPOS_CAJEROS,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                );
        return c;
    }
}
