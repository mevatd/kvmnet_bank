package com.kvmnet.kvmnetbank;

import java.util.ArrayList;
import java.util.List;

public class AccountData {
    static List ACCOUNTS = new ArrayList<String>();
    static List CURRENCIES = new ArrayList<String>();
    static {
        ACCOUNTS.add(
                "2222 2222 22 2222222222"

        );

        ACCOUNTS.add(
                "3333 3333 33 3333333333"

        );

        ACCOUNTS.add(
                "4444 444 44 44444444444"

        );

        ACCOUNTS.add(
                "5555 5555 55 5555555555"

        );
    }

    static{
        CURRENCIES.add("€");
        CURRENCIES.add("$");
    }
}
