package com.kvmnet.kvmnetbank.Interfaces;

import com.kvmnet.kvmnetbank.pojo.Cuenta;

public interface AccountClickListener {
    void onAccountClick(Cuenta cuenta);
}
