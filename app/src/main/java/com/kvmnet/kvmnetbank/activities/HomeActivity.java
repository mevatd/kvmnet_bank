package com.kvmnet.kvmnetbank.activities;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kvmnet.kvmnetbank.Interfaces.AccountClickListener;
import com.kvmnet.kvmnetbank.R;
import com.kvmnet.kvmnetbank.bd.MiBancoOperacional;
import com.kvmnet.kvmnetbank.fragments.AccountsFragment;
import com.kvmnet.kvmnetbank.fragments.TransactionsFragment;
import com.kvmnet.kvmnetbank.pojo.Cuenta;

public class HomeActivity extends AppCompatActivity implements AccountClickListener {

    AccountsFragment accountsFragment;
    MiBancoOperacional mbo;

    TransactionsFragment transactionsFragment;
    Guideline guideline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        guideline = findViewById(R.id.guideline);

        transactionsFragment = (TransactionsFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_transactions);

        accountsFragment = (AccountsFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_accounts);
        accountsFragment.setOnAccountClickListener(this);

        mbo = MiBancoOperacional.getInstance(this);
    }

    @Override
    public void onAccountClick(Cuenta cuenta) {

        boolean oneFragment = (getSupportFragmentManager().findFragmentById(R.id.fragment_transactions) == null);
        if (oneFragment) {
            Bundle bundle = new Bundle();
            Intent intent;


            bundle.putSerializable("Cuenta", cuenta);

            intent = new Intent(this, TransactionsActivity.class);

            intent.putExtras(bundle);

            startActivity(intent);
        } else {
            ((TransactionsFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_transactions)).getTransactions(cuenta);

            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams)guideline.getLayoutParams();
            params.guidePercent = 0.4f;
            guideline.setLayoutParams(params);

        }

    }
}
