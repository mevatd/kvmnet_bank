package com.kvmnet.kvmnetbank.activities;


import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.kvmnet.kvmnetbank.R;
import com.kvmnet.kvmnetbank.adapters.CajeroCursorAdapter;
import com.kvmnet.kvmnetbank.bd.MiBD;
import com.kvmnet.kvmnetbank.bd.MiBancoOperacional;
import com.kvmnet.kvmnetbank.dao.CajeroDAO;
import com.kvmnet.kvmnetbank.dialogs.AtmDialogInfo;
import com.kvmnet.kvmnetbank.dialogs.TransactionsDialog;


public class AtmActivity extends AppCompatActivity {

    private Cursor cursor;
    private MiBancoOperacional mbo;
    private CajeroCursorAdapter cajeroCursorAdapter;
    private CajeroDAO cajeroDAO;

    private TextView textViewAtmId;
    private TextView textViewAtmDir;
    private TextView textViewAtmLat;
    private TextView textViewAtmLng;


    GridView gridAtm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atm);

        //Get an instance from DB
        mbo = MiBancoOperacional.getInstance(getBaseContext());
        SQLiteDatabase db = MiBD.getInstance(getBaseContext()).getWritableDatabase();
        cajeroDAO = new CajeroDAO(this);


        textViewAtmId = findViewById(R.id.textView_atmId);
        textViewAtmDir = findViewById(R.id.textView_atmDirection);
        textViewAtmLat = findViewById(R.id.textView_atmLat);
        textViewAtmLng = findViewById(R.id.textView_atmLng);


        gridAtm = findViewById(R.id.GridView_atm);

        try {
            cajeroDAO.open();
            cursor = cajeroDAO.getCursor();
            startManagingCursor(cursor);
            cajeroCursorAdapter = new CajeroCursorAdapter(this, cursor);
            gridAtm.setAdapter(cajeroCursorAdapter);
            if (cursor.getCount() < 1) {
                Toast.makeText(this, "No data", Toast.LENGTH_SHORT).show();
            } else {
                gridAtm.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        cursor = (Cursor) parent.getItemAtPosition(position);

                        FragmentManager fragmentManager = getSupportFragmentManager();


                        //Transactions dialog new instance constructor requires a Movimiento object to be passed
                        //this way we show the data inside the transactionsdialog
                        AtmDialogInfo atmDialogInfo = AtmDialogInfo.newInstance(cursor, id);

                        atmDialogInfo.show(fragmentManager, "atm");


                    }


                });
            }
        } catch (Exception e) {
            Toast.makeText(getBaseContext(), "An error has occurred", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }




    }


    public void onUpdate(){

        // no va perque no vol
        //gridAtm.invalidateViews();


        //tampoc va perque no vol
        //cajeroCursorAdapter.notifyDataSetChanged();
        //gridAtm.setAdapter(cajeroCursorAdapter);

        //Activity.recreate();
    }
}
