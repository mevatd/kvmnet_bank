package com.kvmnet.kvmnetbank.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kvmnet.kvmnetbank.R;
import com.kvmnet.kvmnetbank.bd.MiBancoOperacional;
import com.kvmnet.kvmnetbank.pojo.Cliente;


public class ChangePassActivity extends AppCompatActivity implements View.OnClickListener {

    EditText userNewPass;
    EditText userNewPass2;

    Button btnApply;
    Cliente cliente;

    String newPass;
    String newPass2;

    MiBancoOperacional mbo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);

        userNewPass = findViewById(R.id.txtUserNewPass);
        userNewPass2 = findViewById(R.id.txtUserNewPass2);

        //Get EditText strings for better usage


        btnApply = findViewById(R.id.btnApply);
        btnApply.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        cliente = (Cliente) bundle.getSerializable("Client");


    }

    @Override
    public void onClick(View v) {
        newPass = userNewPass.getText().toString();
        newPass2 = userNewPass2.getText().toString();

        System.out.println(newPass + " " + newPass2);

        if (newPass.isEmpty() || newPass2.isEmpty()) {
            Toast.makeText(this, "Password cannot be empty", Toast.LENGTH_SHORT).show();
        }else if (!newPass.equals(newPass2)) {
            Toast.makeText(this, "Password mismatch", Toast.LENGTH_SHORT).show();
        }else if (newPass.equals(newPass2)){
            mbo = MiBancoOperacional.getInstance(this);

            cliente.setClaveSeguridad(userNewPass.getText().toString());

            int changed = mbo.changePassword(cliente);

            if (changed == 1){

                Toast.makeText(this, "Password being changed, please wait...", Toast.LENGTH_SHORT).show();
                
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(this, LandingActivity.class);
                startActivity(intent);

                finish();
            }else if (changed == 0){
                Toast.makeText(this, "An database error has occured", Toast.LENGTH_SHORT).show();
            }

        }else{
            Toast.makeText(this, "An program error has occured", Toast.LENGTH_SHORT).show();
        }



    }
}
