package com.kvmnet.kvmnetbank.activities;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.kvmnet.kvmnetbank.R;
import com.kvmnet.kvmnetbank.bd.MiBancoOperacional;
import com.kvmnet.kvmnetbank.pojo.Cliente;

import java.io.File;
import java.io.IOException;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    SharedPreferences preferences;

    MediaPlayer mediaPlayer;

    EditText userId, userPass;
    Button btnLogin;

    //Singleton for DB usage
    MiBancoOperacional mbo;
    Cliente cliente;

    String nifCliente;
    String claveCliente;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //We first need an instance to shared preferences in order to recover saved values
        preferences = getSharedPreferences("bankPreferences", Context.MODE_PRIVATE);


        //edit text used to identify an user
        userId = findViewById(R.id.txtUserId);
        userPass = findViewById(R.id.txtUserPass);
        btnLogin = findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(this);

        //create singleton to be able to use the DB
        mbo = MiBancoOperacional.getInstance(this);


        if (preferences.getBoolean("startupMusic", true)) {


            Uri loginSong = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getPackageName() + "/" + R.raw.bricks);
            mediaPlayer = new MediaPlayer();
            try {
                mediaPlayer.setDataSource(this, loginSong);
            } catch (IOException e) {
                e.printStackTrace();
            }

            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.start();
                }
            });
            //Async task to not block main thread (GUI in this case)
            mediaPlayer.prepareAsync();

        }
    }


    @Override
    public void onClick(View v) {
        cliente = null;
        //create client object for login
        cliente = new Cliente();

        //Clicking the button changes its background
        btnLogin.setBackgroundResource(R.color.colorPrimaryDark);


        if (v.getId() == R.id.btnLogin) {

            //Get login credentials
            nifCliente = userId.getText().toString();
            claveCliente = userPass.getText().toString();

            //Set login credentials
            cliente.setNif(nifCliente);
            cliente.setClaveSeguridad(claveCliente);

            //Test login credentials
            cliente = mbo.login(cliente);

            //if false then login is successfull
            if (cliente == null) {
                Toast.makeText(this, R.string.toast_login_fail, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, R.string.toast_login_success, Toast.LENGTH_SHORT).show();

                //Release the media player to free resources
                if (mediaPlayer != null){
                    mediaPlayer.release();
                    mediaPlayer = null;
                }



                //create a bundle to put the class Client in
                Bundle bundle = new Bundle();
                bundle.putSerializable("Client", cliente);

                //create intent to start new activity with the bundle inside
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);

                intent.putExtras(bundle);

                startActivity(intent);
                finish();
            }

        }

    }

}
