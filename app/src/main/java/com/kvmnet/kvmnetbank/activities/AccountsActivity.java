package com.kvmnet.kvmnetbank.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.kvmnet.kvmnetbank.R;
import com.kvmnet.kvmnetbank.adapters.UserAccsArrayAdapter;
import com.kvmnet.kvmnetbank.bd.MiBancoOperacional;
import com.kvmnet.kvmnetbank.pojo.Cliente;
import com.kvmnet.kvmnetbank.pojo.Cuenta;

import java.util.ArrayList;

public class AccountsActivity extends AppCompatActivity {

    UserAccsArrayAdapter<Cuenta> userAccAdapter;
    ArrayList<Cuenta> userAccs;

    GridView gridUserAccounts;

    Cliente cliente;
    MiBancoOperacional mbo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts);

        gridUserAccounts = findViewById(R.id.GridView_user_accounts);

        Bundle bundle = getIntent().getExtras();
        cliente = (Cliente) bundle.getSerializable("Client");

        mbo = MiBancoOperacional.getInstance(this);

        userAccs = mbo.getCuentas(cliente);

        userAccAdapter = new UserAccsArrayAdapter<>(this, userAccs);
        gridUserAccounts.setAdapter(userAccAdapter);


        gridUserAccounts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                Intent intent;

                Cuenta cuenta;

                cuenta = mbo.getCuentas(cliente).get(position);

                bundle.putSerializable("Cuenta", cuenta);

                intent = new Intent(parent.getContext(), TransactionsActivity.class);

                intent.putExtras(bundle);

                startActivity(intent);

            }
        });

    }
}
