package com.kvmnet.kvmnetbank.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import com.kvmnet.kvmnetbank.R;

import java.util.ArrayList;

public class ConfigurationActivity extends AppCompatActivity {

    SharedPreferences preferences;

    Switch switchStartupMusic;
    Switch switchStartupVideo;
    Spinner spinnerSystemLanguage;
    ListView listViewDbOrigin;
    TextView textViewDbOrigin;

    String dbOrigin = null;


    Button btnApply;

    //adapters
    ArrayAdapter<String> langAdapter;
    ArrayAdapter<String> dbAdapter;

    //Arrays to fill up info inside spinner and recycler
    ArrayList<String> langs = new ArrayList<>();
    ArrayList<String> dbs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);

        //First of all, mapping of the GUI elements is essential in order to draw and set them correctly
        switchStartupMusic = findViewById(R.id.switch_music_on_startup);
        switchStartupVideo = findViewById(R.id.switch_video_on_startup);
        spinnerSystemLanguage = findViewById(R.id.spinner_system_language);
        listViewDbOrigin = findViewById(R.id.listView_database_origin);
        textViewDbOrigin = findViewById(R.id.textView_database_origin);

        btnApply = findViewById(R.id.btn_apply_configuration);


        //bulk data /*//********************************************
        langs.add("Spanish");
        langs.add("English");
        dbs.add("Local");
        dbs.add("Web");
        //**********************************

        spinnerSystemLanguage = findViewById(R.id.spinner_system_language);
        langAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, langs);
        spinnerSystemLanguage.setAdapter(langAdapter);

        listViewDbOrigin = findViewById(R.id.listView_database_origin);
        dbAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, dbs);
        listViewDbOrigin.setAdapter(dbAdapter);

        //We first need an instance to shared preferences in order to recover saved values
        preferences = getSharedPreferences("bankPreferences", Context.MODE_PRIVATE);


        //Whenever the activity is created, we show the previously shaved values
        switchStartupMusic.setChecked(preferences.getBoolean("startupMusic", false));
        switchStartupVideo.setChecked(preferences.getBoolean("startupVideo", false));
        spinnerSystemLanguage.setSelection(preferences.getInt("systemLang", 0));
        textViewDbOrigin.setText(preferences.getString("dbOrigin", "Local"));


        listViewDbOrigin.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //get selected origin source for database
                dbOrigin = (String) parent.getItemAtPosition(position);

            }
        });


        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = preferences.edit();

                editor.putBoolean("startupMusic", switchStartupMusic.isChecked());
                editor.putBoolean("startupVideo", switchStartupVideo.isChecked());
                editor.putInt("systemLang", spinnerSystemLanguage.getSelectedItemPosition());
                editor.putString("dbOrigin", dbOrigin);


                switch (spinnerSystemLanguage.getSelectedItemPosition()) {
                    case 0:
                        setLocale("es");
                        break;
                    case 1:
                        setLocale("en");
                        break;
                }

                editor.apply();

            }
        });
    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
}
