package com.kvmnet.kvmnetbank.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.kvmnet.kvmnetbank.R;

public class LandingActivity extends AppCompatActivity {

    //handler used for delaying the start of the next activity
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        //initialize handler and call runnable
        handler = new Handler();
        handler.postDelayed(runMainActivity, 1000);
    }


    //runnable that starts next activity after the delay of the handler
    private Runnable runMainActivity = new Runnable() {
        public void run() {
            Intent intent = new Intent(LandingActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    };

}
