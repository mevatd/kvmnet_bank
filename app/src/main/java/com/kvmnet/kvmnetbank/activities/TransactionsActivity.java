package com.kvmnet.kvmnetbank.activities;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import com.kvmnet.kvmnetbank.R;
import com.kvmnet.kvmnetbank.adapters.AccTransactionsArrayAdapter;
import com.kvmnet.kvmnetbank.bd.MiBancoOperacional;
import com.kvmnet.kvmnetbank.dialogs.TransactionsDialog;
import com.kvmnet.kvmnetbank.pojo.Cuenta;
import com.kvmnet.kvmnetbank.pojo.Movimiento;

import java.util.ArrayList;

public class TransactionsActivity extends AppCompatActivity {

    AccTransactionsArrayAdapter<Movimiento> accTransactionsArrayAdapter;

    ArrayList<Movimiento> userAccTransactions = new ArrayList<>();

    GridView gridAccTrans;

    Cuenta cuenta;
    MiBancoOperacional mbo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);

        gridAccTrans = findViewById(R.id.GridView_user_account_transactions);

        mbo = MiBancoOperacional.getInstance(this);

        Bundle bundle = getIntent().getExtras();

        cuenta = (Cuenta) bundle.getSerializable("Cuenta");

        userAccTransactions = mbo.getMovimientos(cuenta);

        accTransactionsArrayAdapter = new AccTransactionsArrayAdapter<>(this, userAccTransactions);
        gridAccTrans.setAdapter(accTransactionsArrayAdapter);
        gridAccTrans.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                FragmentManager fragmentManager = getSupportFragmentManager();


                //Transactions dialog new instance constructor requires a Movimiento object to be passed
                //this way we show the data inside the transactionsdialog
                TransactionsDialog transactionsDialog = TransactionsDialog.newInstance(userAccTransactions.get(position));

                transactionsDialog.show(fragmentManager, "transaction");


            }
        });

    }
}
