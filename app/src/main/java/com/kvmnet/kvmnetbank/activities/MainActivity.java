package com.kvmnet.kvmnetbank.activities;


import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kvmnet.kvmnetbank.R;
import com.kvmnet.kvmnetbank.TransferActivity;
import com.kvmnet.kvmnetbank.bd.MiBancoOperacional;
import com.kvmnet.kvmnetbank.dialogs.TransactionsDialog;
import com.kvmnet.kvmnetbank.pojo.Cliente;
import com.kvmnet.kvmnetbank.pojo.Cuenta;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static int MAIN_RQ_CLOSE = 1;

    //Layouts that act as a buttons
    private static final int LoIDs[] =
            {
                    R.id.LoBtnAccounts,
                    R.id.LoBtnAtm,
                    R.id.LoBtnChangePassword,
                    R.id.LoBtnCreditCards,
                    R.id.LoBtnHome,
                    R.id.LoBtnLegalWarning,
                    R.id.LoBtnSignOut,
                    R.id.LoBtnStockMarket,
                    R.id.LoBtnTransactions,
                    R.id.LoBtnTransfer
            };


    Toolbar toolbar;

    //Buttons are linear layouts composed by imageview and textview
    private LinearLayout[] buttons = new LinearLayout[LoIDs.length];


    TextView lblWelcome;
    TextView lblUserAcc;
    TextView lblBalanceAmount;

    Cliente cliente;
    Cuenta cuenta;
    String userName;

    MiBancoOperacional mbo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);


        mbo = MiBancoOperacional.getInstance(this);

        for (int i = 0; i < LoIDs.length; i++) {
            buttons[i] = findViewById(LoIDs[i]);
            buttons[i].setOnClickListener(this);
        }

        //A bundle should be received
        Bundle bundle = this.getIntent().getExtras();
        if (bundle != null) {
            cliente = new Cliente();
            cliente = (Cliente) bundle.getSerializable("Client");
            userName = cliente.getNombre();

            //load a default bank account for the user
            cuenta = mbo.getCuentas(cliente).get(0);


        }


        lblWelcome = findViewById(R.id.LblWelcome);

        lblUserAcc = findViewById(R.id.TextView_user_account);
        lblBalanceAmount = findViewById(R.id.LblBalanceAmount);

        lblUserAcc.setText(cuenta.getNumeroCuenta());
        lblBalanceAmount.setText(String.valueOf(cuenta.getSaldoActual()));

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(lblWelcome.getText() + " " + userName);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_menu, menu);

        for (int i = 0; i < menu.size(); i++) {
            for (int x = 0; x < menu.getItem(i).getSubMenu().size(); x++) {

                Drawable drawable = menu.getItem(i).getSubMenu().getItem(x).getIcon();
                if (drawable != null) {
                    drawable.mutate();
                    drawable.setColorFilter(getResources().getColor(R.color.colorPrimary, getTheme()), PorterDuff.Mode.SRC_ATOP);

                }
            }

        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent;
        Bundle bundle = new Bundle();

        switch (item.getItemId()) {
            case R.id.action_accounts:
                bundle.putSerializable("Client", cliente);

                intent = new Intent(this, AccountsActivity.class);

                intent.putExtras(bundle);

                startActivity(intent);
                break;

            case R.id.action_atm:
                intent = new Intent(this, AtmActivity.class);

                startActivity(intent);

                break;

            case R.id.action_change_pass:
                bundle.putSerializable("Client", cliente);

                intent = new Intent(this, ChangePassActivity.class);

                intent.putExtras(bundle);

                startActivityForResult(intent, MAIN_RQ_CLOSE);
                break;

            case R.id.action_home:
                bundle.putSerializable("Client", cliente);

                intent = new Intent(this, HomeActivity.class);

                intent.putExtras(bundle);

                startActivity(intent);
                break;

            case R.id.action_legal_warning:
                intent = new Intent(this, LegalWarningActivity.class);
                startActivity(intent);
                break;
            case R.id.action_transactions:
                bundle.putSerializable("Cuenta", cuenta);

                intent = new Intent(this, TransactionsActivity.class);

                intent.putExtras(bundle);

                startActivity(intent);
                break;

            case R.id.action_configuration:
                intent = new Intent(this, ConfigurationActivity.class);

                startActivity(intent);
                break;

        }
        return true;
    }


    //if change password finishes, it finishes this activity also in order to clear the activities stack
    //otherwise user will still be logged in
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MAIN_RQ_CLOSE) {
            finish();
        }
    }


    @Override
    public void onClick(View v) {

        Intent intent;
        Bundle bundle = new Bundle();
        switch (v.getId()) {
            case R.id.LoBtnAccounts:
                bundle.putSerializable("Client", cliente);

                intent = new Intent(this, AccountsActivity.class);

                intent.putExtras(bundle);

                startActivity(intent);
                break;

            case R.id.LoBtnAtm:
                intent = new Intent(this, AtmActivity.class);

                startActivity(intent);

                break;

            case R.id.LoBtnChangePassword:
                bundle.putSerializable("Client", cliente);

                intent = new Intent(this, ChangePassActivity.class);

                intent.putExtras(bundle);

                startActivityForResult(intent, MAIN_RQ_CLOSE);

                break;

            case R.id.LoBtnHome:
                bundle.putSerializable("Client", cliente);

                intent = new Intent(this, HomeActivity.class);

                intent.putExtras(bundle);

                startActivity(intent);
                break;

            case R.id.LoBtnLegalWarning:
                intent = new Intent(this, LegalWarningActivity.class);
                startActivity(intent);
                break;

            case R.id.LoBtnSignOut:
                finishAndRemoveTask();
                break;

            case R.id.LoBtnTransactions:
                bundle.putSerializable("Cuenta", cuenta);

                intent = new Intent(this, TransactionsActivity.class);

                intent.putExtras(bundle);

                startActivity(intent);
                break;

            case R.id.LoBtnTransfer:
                bundle.putSerializable("Client", cliente);

                intent = new Intent(this, TransferActivity.class);

                intent.putExtras(bundle);

                startActivity(intent);
                break;

            default:
                Toast.makeText(this, "Not implemented yet", Toast.LENGTH_SHORT).show();

        }
    }
}

