package com.kvmnet.kvmnetbank.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.kvmnet.kvmnetbank.Interfaces.AccountClickListener;
import com.kvmnet.kvmnetbank.R;
import com.kvmnet.kvmnetbank.activities.TransactionsActivity;
import com.kvmnet.kvmnetbank.adapters.UserAccsArrayAdapter;
import com.kvmnet.kvmnetbank.bd.MiBancoOperacional;
import com.kvmnet.kvmnetbank.pojo.Cliente;
import com.kvmnet.kvmnetbank.pojo.Cuenta;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AccountsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AccountsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AccountsFragment extends Fragment implements AdapterView.OnItemClickListener{

    AccountClickListener listener;

    UserAccsArrayAdapter<Cuenta> userAccAdapter;
    ArrayList<Cuenta> userAccs;

    GridView gridUserAccounts;

    Cliente cliente;
    MiBancoOperacional mbo;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        gridUserAccounts = getView().findViewById(R.id.fragment_GridView_user_accounts);

        Bundle bundle = getActivity().getIntent().getExtras();
        cliente = (Cliente) bundle.getSerializable("Client");

        mbo = MiBancoOperacional.getInstance(getContext());

        userAccs = mbo.getCuentas(cliente);

        userAccAdapter = new UserAccsArrayAdapter<>(getContext(), userAccs);
        gridUserAccounts.setAdapter(userAccAdapter);

        gridUserAccounts.setOnItemClickListener(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_accounts, container, false);

    }

    public void setOnAccountClickListener(AccountClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (listener != null){
            listener.onAccountClick((Cuenta) gridUserAccounts.getAdapter().getItem(position));
        }
    }
}
