package com.kvmnet.kvmnetbank.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.kvmnet.kvmnetbank.R;
import com.kvmnet.kvmnetbank.adapters.AccTransactionsArrayAdapter;
import com.kvmnet.kvmnetbank.bd.MiBancoOperacional;
import com.kvmnet.kvmnetbank.dialogs.TransactionsDialog;
import com.kvmnet.kvmnetbank.pojo.Cuenta;
import com.kvmnet.kvmnetbank.pojo.Movimiento;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TransactionsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TransactionsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TransactionsFragment extends Fragment {


    AccTransactionsArrayAdapter<Movimiento> accTransactionsArrayAdapter;

    ArrayList<Movimiento> userAccTransactions = new ArrayList<>();

    GridView gridAccTrans;

    MiBancoOperacional mbo;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transactions, container, false);
    }

    public void getTransactions(Cuenta cuenta){

        gridAccTrans = getView().findViewById(R.id.fragment_GridView_user_account_transactions);

        mbo = MiBancoOperacional.getInstance(getContext());

        userAccTransactions = mbo.getMovimientos(cuenta);

        accTransactionsArrayAdapter = new AccTransactionsArrayAdapter<>(getContext(), userAccTransactions);
        gridAccTrans.setAdapter(accTransactionsArrayAdapter);
        gridAccTrans.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                FragmentManager fragmentManager = getFragmentManager();

                //Transactions dialog new instance constructor requires a Movimiento object to be passed
                //this way we show the data inside the transactionsdialog
                TransactionsDialog transactionsDialog = TransactionsDialog.newInstance(userAccTransactions.get(position));

                transactionsDialog.show(fragmentManager, "transaction");


            }
        });
    }


}
